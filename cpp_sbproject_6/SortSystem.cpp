#include <iostream>
#include <string>
using namespace std;

class ScorePlayers
{
public:
	string player;
	int score;

	ScorePlayers(string p, int s)
	{
		player = p;
		score = s;
	}	
};

void swap(ScorePlayers& p1, ScorePlayers& p2)
{
	ScorePlayers temp = p1;
	p1 = p2;
	p2 = temp;
}

void bubbleSort(ScorePlayers arr[], int n)
{
	for(int i = 0; i < n - 1; i++)
	{
		for (int j = 0; j < n - i - 1; j++)
		{
			if (arr[j].score > arr[j + 1].score)
			{
				swap(arr[j], arr[j + 1]);
			}
		}
	}
}


int main()
{
	ScorePlayers player[]
	{ 
		ScorePlayers("VOV4IK", 1488), 
		ScorePlayers("Godbless", 228), 
		ScorePlayers("fLoW", 69), 
		ScorePlayers("2pac", 1337) 
	};

	int n = sizeof(player) / sizeof(player[0]);

	bubbleSort(player, n);

	for (int i = 0; i < n; i++) 
	{
		std::cout << player[i].player << ": " << player[i].score << endl;
	}

	return 0;
}